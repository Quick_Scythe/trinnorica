package io.github.trinnorica.objects.tools;

import java.awt.Rectangle;

import io.github.trinnorica.Main;
import io.github.trinnorica.entity.Entity;
import io.github.trinnorica.entity.Player;
import io.github.trinnorica.utils.DamageReason;
import io.github.trinnorica.utils.Utils;
import io.github.trinnorica.utils.particles.Particle;
import io.github.trinnorica.utils.sprites.Projectile;
import io.github.trinnorica.utils.sprites.Sprite;
import io.github.trinnorica.utils.sprites.Tool;
import io.github.trinnorica.utils.sprites.ToolType;
import res.ExternalFile;

public class Stick extends Tool{

	public Stick(int x, int y, ToolType type) {
		super(x, y, type);
		power = 1;
		init();
		
	}
	public void init(){
		loadImage(ExternalFile.loadTexture("objects/tools/stick.png"));
		setImageDimensions(16, 16);
		registerXBounds();
	}
	
	public int getWidth(){
		return 27;
	}
	public int getHeight(){
		return 27;
		
	}
	
	public void use(int x, int y) {
		super.use(x, y);
		if(getUser() instanceof Player){
			for (Sprite s : Main.getScreen().objects) {
				if (!(s instanceof Particle || s instanceof Projectile))
					if (getStrikeRange().getBounds().intersects(s.getPolygon().getBounds())) {
						if (s instanceof Entity){
							Rectangle rec = getStrikeRange().getBounds().intersection(s.getPolygon().getBounds());
							Main.addHitPoint(rec.x+(rec.width/2), rec.y+(rec.height/2));
							((Entity) s).damage(getPower(), DamageReason.ENEMY, getUser(), true, false);
						}
					}
			}
		}
		else {
			if (getStrikeRange().getBounds().intersects(Main.getPlayer().getPolygon().getBounds())) {
				Main.getPlayer().damage(getPower(), DamageReason.ENEMY, getUser(), false, true);
			}
		}
		
		
		
	}

}
