package io.github.trinnorica.objects;

import io.github.trinnorica.utils.Direction;
import io.github.trinnorica.utils.sprites.PartialCollidable;
import res.ExternalFile;

public class Trampoline extends PartialCollidable {
	
	public Trampoline(int x, int y) {
		super(x, y);
		init();
	}

	private void init() {
		loadImage(ExternalFile.loadTexture("objects/trampoline.png"));
		addCollidableDirection(Direction.DOWN);
		setImageDimensions(30, 6);
	}
	

}
